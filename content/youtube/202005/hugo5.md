---
title: 'Hugo CMS #5 - CI/CD Gitlab Deployando nuestra Web'
date: '2020-05-07 10:35:00'
type: 'youtube'
category: 'curso'
tags: ["youtube", "hugo", "cms", "gitlab", "deploy", "curso", "tutorial"]
code: 'Sy4KmAQsNZk'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/t4hycjFb/Sy4-Km-AQs-NZkmq.jpg'
---

En esta oportunidad traemos como deployar nuestro sitio web en **GitLab Pages**.

{{< br >}}
{{< link url="https://patojad.com.ar/programacion/2020/05/hugo-cms-ci/cd-deploy-en-gitlab/" text="Ver en el Blog" >}}
{{< br >}}
