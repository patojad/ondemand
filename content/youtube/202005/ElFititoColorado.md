---
title: 'El Fitito Colorado - Cover'
date: '2020-06-07 19:33:00'
type: 'youtube'
category: 'musica'
tags: ["musica", "rock", "acustico", "fitito", "colorado", "pastillas", "abuelo", "lpa"]
code: '3KzeE5x6HI8'
channelid: "UCx-LzLkl_Rda23kuLD5-o7g"
img: 'https://i.postimg.cc/5yx71j9B/3-Kze-E5x6-HI8hd.jpg'
---
Un Temaso de **Las Pastillas Del Abuelo**, versionado. Este tema es acustico, o al menos la version que conozco.
