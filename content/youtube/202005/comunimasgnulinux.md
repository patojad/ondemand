---
title: '¿Comunidades Toxicas? El caso de Mas GNU/Linux'
date: '2020-05-31 10:10:00'
type: 'youtube'
category: 'comedia'
tags: ["youtube", "linux", "comunidades", "comunidad", "toxica", "pedro", "maslinux","masgnulinux", "critica", "comedia"]
code: '8_4dW-NLEN0'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/VNXy7Q0t/8-4d-W-NLEN0hd.jpg'
---

Una grata me llevo al intentar generar disturbios en la comunidad de Mas GNU/Linux y demostrar que existen comunidades que siguen siendo nobles... Como siempre digo hay que mostrar los 2 lados de la moneda!

{{< br >}}
{{< link url="https://patojad.com.ar/noticias/2020/05/comunidades-toxicas-el-caso-de-mas-gnu/linux/" text="Ver en el Blog" >}}
{{< br >}}
