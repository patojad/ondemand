---
title: 'Hugo CMS #2 - Partials y BaseOf'
date: '2020-04-02 11:43:00'
type: 'youtube'
category: 'curso'
tags: ["youtube", "hugo", "cms", "partials", "baseof", "curso", "tutorial"]
code: 'mdjb-e6u8pk'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/90tKpWvg/mdjb-e6u8pkmq.jpg'
---

Ya teniendo nuestro sitio instalado podemos comenzar a modelar un theme para poder acomodarlo a nuestras necesidades. Aquí comenzamos a ver las herramientas que tenemos disponible para llevarlo a cabo.

{{< br >}}
{{< link url="https://patojad.com.ar/programacion/2020/04/hugo-cms-partials-y-baseof/" text="Ver en el Blog" >}}
{{< br >}}
