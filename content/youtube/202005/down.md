---
title: 'Down - Guasones (Acústico)'
date: '2020-05-25 13:33:00'
type: 'youtube'
category: 'musica'
tags: ["musica", "rock", "acustico", "down", "guasones"]
code: 'CaGIHH00BKo'
channelid: "UCx-LzLkl_Rda23kuLD5-o7g"
img: 'https://i.postimg.cc/rF7mP4wt/Ca-GIHH00-BKomq.jpg'
---

Un cover, o mejor dicho una **version** de un temazo de Guasones una banda que me acompaño mucho durante mi adolescencia...
