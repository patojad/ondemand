---
title: 'Hugo CMS #1 - Instalación y Creación del Proyecto'
date: '2020-03-30 08:33:00'
type: 'youtube'
category: 'curso'
tags: ["youtube", "hugo", "cms", "instalacion", "creacion", "curso", "tutorial"]
code: 'ezeWY2RLUXY'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/qR25JJMB/eze-WY2-RLUXYmq.jpg'
---

Bienvenidos al *"curso"* de **HugoCMS**, vamos a ir aprendiendo de a poco como crear nuestro sitio con este CMS estático de excelencia!

{{< br >}}
{{< link url="https://patojad.com.ar/programacion/2020/03/hugo-cms-instalaci%C3%B3n-y-creaci%C3%B3n-del-proyecto/" text="Ver en el Blog" >}}
{{< br >}}
