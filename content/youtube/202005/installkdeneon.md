---
title: 'Instalacion KDE Neon'
date: '2019-01-20 19:33:00'
type: 'youtube'
category: 'instalacion'
tags: ["youtube", "kde", "neon", "español", "ubuntu", "instalacion"]
code: 'gAP7y2pEC1M'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/zGJgKs3d/g-AP7y2p-EC1-Mhd.jpg'
---

Instalacion de KDE Neon una distro base ubuntu con lo mejor compatibilidad a su entorno KDE

{{< br >}}
{{< link url="https://patojad.com.ar/instalaciones/ubuntu/kdeneon/" text="Ver en el Blog" >}}
{{< br >}}
