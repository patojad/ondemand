---
title: 'Crear un .deb con DPKG'
date: '2020-04-03 08:33:00'
type: 'youtube'
category: 'tutorial'
tags: ["youtube", "debian", "ubuntu", "dpk", "deb", "creacion", "tutorial"]
code: 'mHqRbo1A5MM'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/SNN3bY3D/m-Hq-Rbo1-A5-MMmq.jpg'
---

Bienvenidos al *"curso"* de **HugoCMS**, vamos a ir aprendiendo de a poco como crear nuestro sitio con este CMS estático de excelencia!

{{< br >}}
{{< link url="https://patojad.com.ar/programacion/2020/04/como-crear-un-deb-con-dpkg/" text="Ver en el Blog" >}}
{{< br >}}
