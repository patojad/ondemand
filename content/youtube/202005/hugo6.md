---
title: 'Hugo CMS #6 - Taxonomias y Metadata'
date: '2020-05-12 12:35:00'
type: 'youtube'
category: 'curso'
tags: ["youtube", "hugo", "cms", "taxonomias", "metadata", "curso", "tutorial"]
code: 'lZ63cdVJk2c'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/BbYtXGMk/l-Z63cd-VJk2cmq.jpg'
---

En esta oportunidad vamos a ver un poco las taxonomias y vamos a trabajar con la metadata.

{{< br >}}
{{< link url="https://patojad.com.ar/programacion/2020/05/hugo-cms-taxonomias-y-metadata/" text="Ver en el Blog" >}}
{{< br >}}
