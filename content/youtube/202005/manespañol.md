---
title: 'Traducir man al Español'
date: '2019-01-13 09:33:00'
type: 'youtube'
category: 'tutorial'
tags: ["youtube", "man", "linux", "español", "tutorial"]
code: 'nLNnuyBsxAg'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/8zVgFs6Q/n-LNnuy-Bsx-Aghd.jpg'
---

En esta oportunidad vamos a ver como traducir man **el manual de linux** al español.

{{< br >}}
{{< link url="https://patojad.com.ar/linux/2019/01/poner-las-paginas-de-man-en-espa%C3%B1ol/" text="Ver en el Blog" >}}
{{< br >}}
