---
title: 'Hugo CMS #4 - Date en español'
date: '2020-04-15 10:35:00'
type: 'youtube'
category: 'curso'
tags: ["youtube", "hugo", "cms", "date", "español", "curso", "tutorial"]
code: 'NfGVnzbcZ-g'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/Ssw-k5N1y/Nf-GVnzbc-Z-gmq.jpg'
---

En este 4 capitulo estamos dandole a la fecha el formato que deseamos en el idioma que deseamos. Una mención especial a **Tehuel** quien es la persona que me paso este *"hack"*

{{< br >}}
{{< link url="https://patojad.com.ar/programacion/2020/04/hugo-cms-date-en-espa%C3%B1ol/" text="Ver en el Blog" >}}
{{< br >}}
