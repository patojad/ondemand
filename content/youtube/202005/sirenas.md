---
title: 'Sirenas (Acustico)'
date: '2020-03-28 16:33:00'
type: 'youtube'
category: 'musica'
tags: ["musica", "rock", "acustico", "sirenas", "surfake"]
code: 'TRb8K6nmhDc'
channelid: "UCx-LzLkl_Rda23kuLD5-o7g"
img: 'https://i.postimg.cc/5yMggyFc/TRb8-K6nmh-Dchd.jpg'
---

Es un tema de la banda **Surfake**, (banda que ya no existe) el cual me encanta. El tema Fue escrito por mi hermano **Guille** (https://www.instagram.com/guillofake/ pueden ver sus covers o canciones aquí)
