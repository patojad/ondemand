---
title: 'Left 4 Dead 2 - No Mercy #4 (Un tropezón no es caída)'
date: '2020-04-16 12:35:00'
type: 'youtube'
category: 'juegos'
tags: ["youtube", "l4d", "left", "4", "dead", "2", "no", "mercy", "juegos", "gameplay"]
code: 'l_SiYIL1oAQ'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/Dwyq6wqR/l-Si-YIL1o-AQhd.jpg'
---

Llegamos con **WayNe** y **Mitiken** al falso final, se nos complica salir del hospital y el humor esta por el piso... ¿Llegamos a terminar?
