---
title: 'Hugo CMS #3 - Cards y Range'
date: '2020-04-06 10:35:00'
type: 'youtube'
category: 'curso'
tags: ["youtube", "hugo", "cms", "card", "range", "curso", "tutorial"]
code: 'ZJ-0n1OGam0'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/Z5VZmLss/ZJ-0n1-OGam0mq.jpg'
---

En este caso vamos a estar agregando el primer nivel de dinamismo del sitio. Esto nos va a comenzar a dar un mejor gustito a nuestro sitio.

{{< br >}}
{{< link url="https://patojad.com.ar/programacion/2020/04/hugo-cms-cards-y-range/" text="Ver en el Blog" >}}
{{< br >}}
