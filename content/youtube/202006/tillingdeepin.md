---
title: 'Tillin en kwin (Deepin)'
date: '2020-06-08 10:10:00'
type: 'youtube'
category: 'tutorial'
tags: ["youtube", "linux", "tilling", "deepin", "kwin", "grilla", "tutorial"]
code: 'rGom75QA_HA'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/Ss6FR7NM/r-Gom75-QA-HAhd.jpg'
---

Vamos a ver en esta oportunidad como instalar el efecto tilling en nuestro escritorio con kwin, en esta oportunidad vamos a hacerlo en Deepin.

{{< br >}}
{{< link url="https://patojad.com.ar/estilo/2020/06/tilling-en-kwin-deepin/" text="Ver en el Blog" >}}
{{< br >}}
