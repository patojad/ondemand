---
title: 'Hugo CMS #7 - Un Buscador en nuestro sitio!'
date: '2020-06-11 08:02:00'
type: 'youtube'
category: 'curso'
tags: ["youtube", "hugo", "cms", "buscador", "search", "curso", "tutorial"]
code: '-IGOrQpXVM8'
channelid: "UCta4Iy4TzMx8Xo0pwpkbWgg"
img: 'https://i.postimg.cc/50tTSNG2/mqdefault.jpg'
---

Nuevamente gracias a **Tehuel** un grande y gran amigo de la comunidad les traemos en este capitulo como crear o agregar un buscador para nuestro sitio, es completamente funcional y muy útil.

{{< br >}}
{{< link url="https://patojad.com.ar/programacion/2020/06/hugo-cms-un-buscador-en-nuestro-sitio/" text="Ver en el Blog" >}}
{{< br >}}
